#include "FrameProcessor.hpp"
#include "utils.hpp"


FrameProcessor::FrameProcessor():
    thresh(100),max(255),type(1),time(0),
    skipCount(-2)
{

}

FrameProcessor::~FrameProcessor(){}


void FrameProcessor::preProcess()
{	
	std::vector<cv::Mat> channels;
	cv::Mat hsv;
    cv::cvtColor( original, hsv, CV_RGB2HSV );
	cv::split(hsv, channels);
    original = channels[2];
// 	cv::cvtColor(output(roi),output, CV_RGB2GRAY);
	
    cv::threshold(original,frame,thresh,max,type);
// 	cv::Canny(output,output,40,100);
//	adaptiveThreshold(output,output,255,cv::ADAPTIVE_THRESH_GAUSSIAN_C,cv::THRESH_BINARY_INV,3,5);
}

void FrameProcessor::nextFrame(cv::Mat f, int t)
{
    if(skipCount>0)
    {
        skipCount--;
        return;
    }

    original  = f.clone();
    time = t;


    preProcess();

    if(skipCount == -2)
    {
        db.addEntry(frame,original,time);
        oldFrame = frame.clone();
        skipCount = -1;
        return;
    }

    double diff = getDiff(oldFrame,frame);
    if(skipCount == -1)
    {
        if (diff > 4500 )
        {
            skipCount = 15;
        }
    }
    else if(skipCount == 0)
    {
        if (diff > 5300 )
        {
            db.addEntry(frame,original,time);

            oldFrame = frame.clone();
        }
        skipCount = -1;
    }
}



ImageDatabase FrameProcessor::getDB()
{
    return db;
}

void FrameProcessor::end(int t)
{
    db.setEndTime(t);
}

