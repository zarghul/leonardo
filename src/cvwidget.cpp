#include "cvwidget.h"
#include "utils.hpp"
#include <utility>

CVWidget::CVWidget(QWidget *parent): QWidget(parent)
{

}

void CVWidget::showImage(int pos)
{
    cap.set(CV_CAP_PROP_POS_FRAMES,pos);
    cv::Mat image;
    cap >> image;

    _qimage = cvMatToQImage(image);

    //this->setFixedSize(image.cols, image.rows);

    repaint();
}

void CVWidget::paintEvent(QPaintEvent* /*event*/)
{
    QPainter painter(this);
    // Display the image
    if(!_qimage.isNull())
        painter.drawImage(QPoint(0,0), _qimage.scaled(this->width(),this->height()));
    painter.end();
}

bool CVWidget::open(QString file)
{
    if(cap.isOpened())
        cap.release();

    return cap.open(file.toStdString());
}

int CVWidget::getCaptureFrameCount()
{
    return cap.get(CV_CAP_PROP_FRAME_COUNT);
}


void CVWidget::mousePressEvent(QMouseEvent *event)
 {
    repaint();
    origin = event->pos();
    int key;
    if(rect.size()!=0)
        key = rect.rbegin()->first+1;
    else
        key = 0;
    rect.insert(std::make_pair(key,new QRubberBand(QRubberBand::Rectangle,this)));
    rect.at(key)->setGeometry(QRect(origin, QSize()));
    rect.at(key)->show();

    emit newRoi(key);
 }

 void CVWidget::mouseMoveEvent(QMouseEvent *event)
 {
     repaint();
     rect.rbegin()->second->setGeometry(QRect(origin, event->pos()).normalized());
 }

 void CVWidget::mouseReleaseEvent(QMouseEvent *)
 {
     repaint();
 }

// void CVWidget::resizeEvent(QResizeEvent *e)
// {
////     double ratioW = double(e->size().width())/e->oldSize().width();
////     double ratioH = double(e->size().height())/e->oldSize().height();
////     for(auto i : rect)
////     {
////         QSize s = i->size();
////         i->resize(s.width()*ratioW,s.height()*ratioH);

////     }
////     repaint();
// }

 std::map<int, cv::Rect> CVWidget::getRoi()
 {
     std::map<int,cv::Rect> vr;

     double xScale = double(_qimage.width())/this->width();
     double yScale = double(_qimage.height())/this->height();

     for(auto i : rect)
     {
         cv::Rect r;
         r.x = i.second->geometry().topLeft().x()*xScale;
         r.y = i.second->geometry().topLeft().y()*yScale;
         r.width = i.second->width()*xScale;
         r.height = i.second->height()*yScale;
         vr.insert(std::make_pair(i.first,r));
     }

    return vr;
 }

 void CVWidget::deleteRoi(int i)
 {
     rect.at(i)->hide();
     rect.erase(rect.find(i));
 }
