#-------------------------------------------------
#
# Project created by QtCreator 2013-03-27T16:36:17
#
#-------------------------------------------------

CONFIG += qt
QT += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = leonardo
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    videoprocessor.cpp \
    FrameProcessor.cpp \
    ImageDatabase.cpp \
    utils.cpp   \
    cvwidget.cpp

HEADERS  += mainwindow.h \
    videoprocessor.h \
    FrameProcessor.hpp \
    ImageDatabase.hpp \
    utils.hpp   \
    cvwidget.h

FORMS    += mainwindow.ui


unix{
CONFIG += link_pkgconfig
PKGCONFIG += opencv
QMAKE_CXXFLAGS += -std=c++0x
}


RESOURCES += \
    res.qrc

