#ifndef UTILS_HPP
#define UTILS_HPP

#include <string>
#include <opencv2/opencv.hpp>
#include <QImage>

double getDiff(cv::Mat& a,cv::Mat& b);
QImage cvMatToQImage(cv::Mat image);

#endif
