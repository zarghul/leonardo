#ifndef DATABASE_HPP
#define DATABASE_HPP

#include <opencv2/opencv.hpp>
#include <vector>

class ImageDatabase
{
public:
    ImageDatabase();
    ~ImageDatabase();
    void addEntry(cv::Mat image,cv::Mat original,double start);
    void setEndTime(double t);

    struct stats
    {
        double totalTime;
        double percTime;
        double averageTime;
        int occurencies;
    };

    stats getStats(int i);
    cv::Mat getImg(int i);

    int getSize();

private:
    int matchSig(cv::Mat sig);

    std::vector<cv::Mat> signatures;
    std::vector<cv::Mat> originals;
    std::vector<std::vector<std::pair<double,double> > > times;
    double endTime;
    std::pair<int,int> lastEntry;
};

#endif
