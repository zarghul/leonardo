#ifndef VIDEOPROCESSOR_H
#define VIDEOPROCESSOR_H

#include <QThread>
#include <fstream>
#include <vector>
#include <map>
#include <opencv2/opencv.hpp>

#include "ImageDatabase.hpp"
#include "FrameProcessor.hpp"


class VideoProcessor : public QThread
{
    Q_OBJECT
public:
    explicit VideoProcessor();
    ~VideoProcessor();
    
protected:
    void run();

signals:
    void done();
    void progress(int perc);
    
public slots:
    void process();
    void stop();
    void end();

    //-------------------------------
    //da usare a thread inattivo!
    void setSource(QString src);
    void setRoi(std::map<int,cv::Rect> r);
    std::vector<ImageDatabase> getDB();

private:
    cv::VideoCapture cap;

    std::map<int,cv::Rect> roi;
    std::vector<FrameProcessor*> fp;

    QString src;

    int count;
    
};

#endif // VIDEOPROCESSOR_H
