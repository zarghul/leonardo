#include "videoprocessor.h"

#include <QTimer>
#include <iostream>
#include <sstream>

#include "FrameProcessor.hpp"
#include "ImageDatabase.hpp"
#include "utils.hpp"

using namespace std;

VideoProcessor::VideoProcessor():
    QThread(0)
{
    moveToThread(this);
}

VideoProcessor::~VideoProcessor()
{
    for(auto i : fp)
    {
        delete i;
    }
}

void VideoProcessor::run()
{
    QTimer *timer = new QTimer();
    connect(timer, SIGNAL(timeout()),this, SLOT(process()));
    timer->start(0);
    count  = 0;

    cap.open(src.toStdString());

    for(auto i : fp)
    {
        delete i;
    }
    fp.clear();

    for(int i  = 0; i< roi.size(); ++i)
    {
        fp.push_back(new FrameProcessor);
    }

    exec();
}

void VideoProcessor::process()
{
    cv::Mat frame;
    cap >> frame;
    if(frame.empty())
    {
        end();
    }
    int i = 0;
    for(auto r : roi)
    {
        fp.at(i)->nextFrame(frame(r.second),cap.get(CV_CAP_PROP_POS_MSEC));
        i++;
    }

    emit progress((++count)*100/cap.get(CV_CAP_PROP_FRAME_COUNT));
    if(count == cap.get(CV_CAP_PROP_FRAME_COUNT))
    {
        end();
    }
}

void VideoProcessor::stop()
{
    if(currentThread() != this)
    {
        QMetaObject::invokeMethod(this, "stop",Qt::QueuedConnection);
    }
    else
    {
        end();
    }
}

void VideoProcessor::setSource(QString src)
{
    this->src = src;
}

void VideoProcessor::setRoi(std::map<int, cv::Rect> r)
{
    this->roi = r;
}

std::vector<ImageDatabase> VideoProcessor::getDB()
{
    std::vector<ImageDatabase> ret;
    for(auto i : fp)
    {
        ret.push_back(i->getDB());
    }

    return ret;
}

void VideoProcessor::end()
{
    for(auto i : fp)
    {
        i->end(cap.get(CV_CAP_PROP_POS_MSEC));
    }
    emit done();
    quit();
}
