#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "videoprocessor.h"
#include "utils.hpp"

#include <QFileDialog>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(ui->horizontalSlider,SIGNAL(valueChanged(int)),
            ui->cvFrame,SLOT(showImage(int)),
            Qt::QueuedConnection);

    connect(ui->cvFrame, SIGNAL(newRoi(int)),
            this, SLOT(addRoi(int)),
              Qt::QueuedConnection);

    ui->startBtn->setDisabled(true);

    QAction* myAction;
    myAction = new QAction("Elimina", this);
    myAction->setStatusTip("Elimina Area");
    connect(myAction, SIGNAL(triggered()),
            this, SLOT(deleteRoi()),
            Qt::QueuedConnection);

    ui->treeWidget->addAction(myAction);
    ui->treeWidget->setContextMenuPolicy(Qt::ActionsContextMenu);
}
\
MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_startBtn_clicked()
{
    ui->startBtn->setDisabled(true);

    ui->treeWidget->setContextMenuPolicy(Qt::NoContextMenu);

    v = new VideoProcessor();


    ui->progressBar->setValue(0);

    connect(v, SIGNAL(done()),
              this, SLOT(processDone()),
              Qt::QueuedConnection);

    connect(v, SIGNAL(progress(int)),
            ui->progressBar, SLOT(setValue(int)),
              Qt::QueuedConnection);

    connect(v, SIGNAL(progress(int)),
            this, SLOT(estimateTime(int)),
              Qt::QueuedConnection);

    connect(ui->stopBtn, SIGNAL(clicked()),
            v, SLOT(stop()),
              Qt::QueuedConnection);


    for (int i = 0; i < ui->treeWidget->topLevelItemCount(); ++i)
    {
        qDeleteAll(ui->treeWidget->topLevelItem(i)->takeChildren());
    }

    v->setSource(filename);
    v->setRoi(ui->cvFrame->getRoi());
    v->start();
}

void MainWindow::processDone()
{
    ui->startBtn->setDisabled(false);
    ui->treeWidget->setContextMenuPolicy(Qt::ActionsContextMenu);

    std::vector<ImageDatabase> dbv = v->getDB();

    for(size_t
        j = 0; j < dbv.size(); ++j)
    {
        QTreeWidgetItem* top = ui->treeWidget->topLevelItem(j);
        for(int i = 0; i< dbv.at(j).getSize(); i++)
        {

            QTreeWidgetItem* item = new QTreeWidgetItem;
            item->setData(0,Qt::DecorationRole,QPixmap::fromImage(cvMatToQImage(dbv.at(j).getImg(i))));
            item->setWhatsThis(0,QString("%1").arg(i));
            top->addChild(item);

        }
    }

    connect(ui->treeWidget, SIGNAL(itemSelectionChanged()),
            this, SLOT(showStats()),
              Qt::QueuedConnection);

}

void MainWindow::on_actionApri_triggered()
{
    filename = QFileDialog::getOpenFileName(this,"Apri File",
                                                     "",
                                                 "Files (*.*)");

    bool ret = ui->cvFrame->open(filename);
    if(ret)
    {
        int max = ui->cvFrame->getCaptureFrameCount();
        ui->horizontalSlider->setMaximum(max);

        ui->startBtn->setDisabled(false);

        ui->cvFrame->showImage(0);

        ui->horizontalSlider->setValue(0);
    }

}
void MainWindow::showStats()
{
    auto selected = ui->treeWidget->selectedItems();
    ImageDatabase::stats st = {0,0,0,0};
    for(auto e : selected)
    {
        int index = ui->treeWidget->indexOfTopLevelItem(e->parent());
        ImageDatabase::stats tmp = v->getDB().at(index).getStats(e->whatsThis(0).toInt());
        st.totalTime += tmp.totalTime;
        st.percTime += tmp.percTime;
        st.averageTime = (st.averageTime*st.occurencies+tmp.averageTime*tmp.occurencies)/(st.occurencies+tmp.occurencies);
        st.occurencies+=tmp.occurencies;
    }
    QString s = QString("tempo totale: %1 s\n"
                        "tempo percentuale: %2 %\n"
                        "tempo medio: %3 s\n"
                        "numero di occorrenze: %6\n"
                        ).arg(st.totalTime/1000.0,0,'f',0).arg(st.percTime*100.00,0,'f',0).arg(st.averageTime/1000.00,0,'f',0).arg(st.occurencies);

    ui->plainTextEdit->setPlainText(s);
}

void MainWindow::estimateTime(int i)
{
    if(i==0)
    {
        t.start();
    }
    else
    {
        int elapsed = t.elapsed()/1000;
        int sec = elapsed;
        sec *= 100.0/i;
        sec -= elapsed;
        QString s = QString("tempo residuo stimato:  %1:%2:%3\ntempo trascorso:  %4:%5:%6").arg(sec/3600,2,10,QChar('0')).arg((sec%3600)/60,2,10,QChar('0')).arg(sec%60,2,10,QChar('0')).arg(elapsed/3600,2,10,QChar('0')).arg((elapsed%3600)/60,2,10,QChar('0')).arg(elapsed%60,2,10,QChar('0'));
        ui->label->setText(s);
    }
}

void MainWindow::addRoi(int i)
{
    QTreeWidgetItem* item = new QTreeWidgetItem;
    item->setText(0,QString("Area ")+QString("%1").arg(i));
    item->setWhatsThis(0,QString("%1").arg(i));
    item->setFlags(Qt::ItemIsEnabled);
    ui->treeWidget->addTopLevelItem(item);
}

void MainWindow::deleteRoi()
{
    QTreeWidgetItem* item = ui->treeWidget->currentItem();
    if(item->parent() != nullptr)
        item = item->parent();

    int i = item->whatsThis(0).toInt();
    ui->cvFrame->deleteRoi(i);

    int index = ui->treeWidget->indexOfTopLevelItem(item);
    qDeleteAll(ui->treeWidget->topLevelItem(index)->takeChildren());
    delete ui->treeWidget->takeTopLevelItem(index);
}
