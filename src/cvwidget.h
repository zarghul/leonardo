#ifndef CVWIDGET_H
#define CVWIDGET_H

#include <QWidget>
#include <QImage>
#include <QPainter>
#include <QRubberBand>
#include <QMouseEvent>
#include <opencv2/opencv.hpp>

#include <map>

class CVWidget : public QWidget
{
    Q_OBJECT
public:
    explicit CVWidget(QWidget *parent = 0);

    QSize sizeHint() const { return _qimage.size(); }
    QSize minimumSizeHint() const { return _qimage.size(); }
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *);
//    void resizeEvent(QResizeEvent *e);
    
signals:
    void newRoi(int i);
    
public slots:
    void showImage(int pos);
    bool open(QString file);
    int getCaptureFrameCount();
    std::map<int,cv::Rect> getRoi();
    void deleteRoi(int i);

protected:
    void paintEvent(QPaintEvent* /*event*/);

    QImage _qimage;
    cv::Mat _tmp;

    cv::VideoCapture cap;

    std::map<int,QRubberBand*> rect;
    QPoint origin;

};

#endif // CVWIDGET_H
