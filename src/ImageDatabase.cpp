#include <utility>
#include <algorithm>
#include "ImageDatabase.hpp"
#include "utils.hpp"



ImageDatabase::ImageDatabase():endTime(-1),lastEntry(-1,-1)
{
	
}


ImageDatabase::~ImageDatabase()
{

}


void ImageDatabase::addEntry(cv::Mat sig, cv::Mat original, double start)
{
    int elem = this->matchSig(sig);
    if(elem == -1)
	{
        std::vector<std::pair<double,double> > v;
        v.push_back(std::make_pair(start,endTime));
        times.push_back(v);
        signatures.push_back(sig.clone());
        originals.push_back(original.clone());

        if(lastEntry.first!=-1)
        {
            times.at(lastEntry.first).at(lastEntry.second).second = start;
        }
        lastEntry.first = times.size()-1;
        lastEntry.second = 0;
	}
    else
    {
        times.at(lastEntry.first).at(lastEntry.second).second = start;
        times.at(elem).push_back(std::make_pair(start,endTime));

        lastEntry.first = elem;
        lastEntry.second = times.at(elem).size()-1;
    }
}

int ImageDatabase::matchSig(cv::Mat sig)
{
    int i = 0;
    for(auto db_i : signatures)
    {
        if(getDiff(db_i,sig)<4500)
        {
            return i;
        }
        i++;
    }
    return -1;
}

void ImageDatabase::setEndTime(double t)
{
    endTime = t;
    times.at(lastEntry.first).at(lastEntry.second).second = endTime;
}

int ImageDatabase::getSize()
{
    return signatures.size();
}

cv::Mat ImageDatabase::getImg(int i)
{
    return originals.at(i);
}

ImageDatabase::stats ImageDatabase::getStats(int i)
{
    stats s;

    std::vector<double> d;

    for(auto j : times.at(i))
    {
        d.push_back(j.second-j.first);
    }
    s.totalTime = 0;
    for(auto k : d)
    {
        s.totalTime+=k;
    }
    s.percTime = s.totalTime/endTime;
    s.occurencies = d.size();
    s.averageTime = s.totalTime/s.occurencies;


    return s;
}
