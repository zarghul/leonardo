#include <opencv/highgui.h>
#include "utils.hpp"

using namespace std;

double getDiff(cv::Mat& a,cv::Mat& b)
{
	cv::Mat c;
	cv::absdiff(a,b,c);
	
	return cv::norm(c,CV_RELATIVE_L2);
}

QImage cvMatToQImage(cv::Mat image)
{
    cv::Mat _tmp;
    // Convert the image to the RGB888 format
    switch (image.type())
    {
        case CV_8UC1:
            cvtColor(image, _tmp, CV_GRAY2RGB);
            break;
        case CV_8UC3:
            cvtColor(image, _tmp, CV_BGR2RGB);
            break;

        default:
            throw "tipo sconosciuto";
    }

    // QImage needs the data to be stored continuously in memory
    assert(_tmp.isContinuous());
    // Assign OpenCV's image buffer to the QImage. Note that the bytesPerLine parameter
    // (http://qt-project.org/doc/qt-4.8/qimage.html#QImage-6) is 3*width because each pixel
    // has three bytes.
    QImage ret(_tmp.data, _tmp.cols, _tmp.rows, _tmp.cols*3, QImage::Format_RGB888);

    return ret.copy();
}
