#ifndef FRAMEPROCESSOR_HPP
#define FRAMEPROCESSOR_HPP

#include <opencv2/opencv.hpp>
#include <QThread>
#include "ImageDatabase.hpp"

class FrameProcessor
{
public:
    explicit FrameProcessor();
    ~FrameProcessor();
    void nextFrame(cv::Mat f, int t);
    ImageDatabase getDB();
    void end(int t);


private:
    void preProcess();

    cv::Mat oldFrame,frame;
    cv::Mat original;

    ImageDatabase db;

    int time;

    int thresh;
    int max;
    int type;

    int skipCount;

};

#endif
