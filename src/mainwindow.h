#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QListWidgetItem>
#include <QTime>
#include "videoprocessor.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
private slots:
    void on_startBtn_clicked();
    void processDone();
    void on_actionApri_triggered();
    void showStats();
    void estimateTime(int);
    void addRoi(int i);
    void deleteRoi();


private:
    Ui::MainWindow *ui;
    QString filename;
    VideoProcessor* v;

    QTime t;
};

#endif // MAINWINDOW_H
